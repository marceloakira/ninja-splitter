#!/usr/bin/python

import random
import hunspell
import numpy as np
import nltk
from nltk.corpus import mac_morpho
from collections import Counter, defaultdict
from nltk import bigrams, trigrams
# see: https://wiki.python.org/moin/UsingPickle
# import pickle
# dill serializes lambda functions, pickle not
# see: https://stackoverflow.com/questions/25348532/can-python-pickle-lambda-functions
import dill as pickle
from pathlib import Path
import argparse
import sys

class Ninja:
    # stop letters
    spell_dic = {'b':1,'c':1,'d':1,'f':1,'g':1,'h':1,'i':1, \
                    'j':1,'k':1,'l':1,'m':1,'n':1,'p':1,'q':1, \
                    'r':1,'s':1,'t':1,'u':1,'v':1,'w':1,'x':1, \
                    'y':1,'z':1}

    dic = hunspell.HunSpell("/usr/share/hunspell/pt_BR.dic", "/usr/share/hunspell/pt_BR.aff")
  

    def __init__(self, sentence):
        self.sentence = sentence.lower()
        self.mgrams = {}
        self.valids = []
        self.tgrams = {}
        self.spelled_ngrams()
        Ninja.model = Ninja.assert_model(mac_morpho)
        Ninja.frequency = Ninja.assert_frequency()
        Ninja.stopwords = Ninja.assert_stopwords()

    @staticmethod
    def assert_stopwords():
        if Path('stopwords.cache').is_file():
            stopwords = pickle.load( open('stopwords.cache', 'rb'))
        else: 
            print('Generating stopwords.cache, please wait...')
            stopwords = nltk.corpus.stopwords.words('portuguese')
            pickle.dump(stopwords, open('stopwords.cache', 'wb'))
        return stopwords

    @staticmethod
    def assert_frequency():
        if Path('frequency.cache').is_file():
            frequency = pickle.load( open('frequency.cache', 'rb') )
        else:
            print('Generating frequency.cache, please wait....')
            frequency = Counter(mac_morpho.words())
            pickle.dump(frequency, open('frequency.cache', 'wb'))
        return frequency

    # reference: https://nlpforhackers.io/language-models/
    @staticmethod
    def assert_model(corpus):
        if Path('model.cache').is_file():
            model = pickle.load( open('model.cache', 'rb'))
        else:
            print('Generating model.cache, please wait...')
            model = defaultdict(lambda: defaultdict(lambda: 0))
            
            for sentence in corpus.sents():
                for w1, w2 in bigrams(sentence, pad_right=True, pad_left=True):
                    model[(w1)][w2] += 1

            for w1 in model:
                total_count = float(sum(model[w1].values()))
                for w2 in model[w1]:
                    model[w1][w2] /= total_count
            pickle.dump(model, open('model.cache', 'wb'))

        return model
    
    @staticmethod
    def spell(word):
        if Ninja.spell_dic.get(word) == None:
            if Ninja.dic.spell(word):
                Ninja.spell_dic[word] = 0
            else:
                Ninja.spell_dic[word] = 1
        return Ninja.spell_dic[word]

    # tgrams: n-grams tree, stores at j the pointer of next i_gram
    # mgrams: n-grams map, stores at j position valid i_grams
    def spelled_ngrams(self):
        i = 0
        j = 0
        l = len(self.sentence)
        for i in range(1,l+1):
            for j in range(0,l-i+1):
                i_gram = self.sentence[j:j+i]
                if Ninja.spell(i_gram) == 0:
                    if self.mgrams.get(j) == None:
                        self.mgrams[j] = [i_gram]
                        self.tgrams[j] = [j+len(i_gram)]
                    else:
                        self.mgrams[j].append(i_gram)
                        self.tgrams[j].append(j+len(i_gram))

    def valid_sentences(self, node):
        if node not in self.tgrams:
            yield [node]
        else:
            for next_node in self.tgrams[node]:
                for r in self.valid_sentences(next_node):
                    yield [node] + r

    def list_valid_sentences(self, paths):
        result = []
        results = []
        for path in paths:
            if path[-1] == len(self.sentence):
                for i in range(0, len(path)-1):
                    result.append(self.sentence[path[i]:path[i+1]])
                results.append(result)
            result = []
        return results

    @staticmethod
    def eval_freq(words):
        n_rare = 0
        nw = 0
        ns = 0
        for word in words:
            nw = nw + 1
            if word in Ninja.stopwords:
                ns = ns + 1
            elif Ninja.frequency[word]==0:
                n_rare = n_rare + 1
        return 1 - ( (n_rare + ns) / nw )

    @staticmethod
    def eval_model(words):
        n_hits = 0
        l = len(words)
        for i in range(0, l-1):
            m = Ninja.model[words[i]][words[i+1]]
            if m != 0:
                n_hits = n_hits + 1
        return n_hits / l

    @staticmethod
    def eval_(sentence):
        return (Ninja.eval_model(sentence)+Ninja.eval_freq(sentence))/2

    def execute(self, candidates, verbose):
        paths = list(self.valid_sentences(0))
        l = []
        for sentence in self.list_valid_sentences(paths):
            eval_freq = Ninja.eval_freq(sentence)
            eval_model = Ninja.eval_model(sentence)
            eval_ = (eval_freq+eval_model)/2
            l.append((eval_, ' '.join(sentence), eval_freq, eval_model))

        if len(l) == 0:
            return self.sentence

        l.sort()
        if candidates:
            if verbose:
                return l
            else:
                return [ x[1] for x in l ]
        else:
            if verbose:
                return l[-1]
            else:
                return l[-1][1]

class Application:

    def __init__(self, argv):
        self.argv = argv
    
    def usage(self):
        help='''    Ninja Splitter: split compound words
    Execute 'ninja.py -h' for help
        '''
        print(help)

    def run(self):
        if len(sys.argv) == 1:
            self.usage()
            sys.exit(-1)

        parser = argparse.ArgumentParser(description="split compound words")
        parser.add_argument('compound', help="compound word",
                            )
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-c', dest='candidates', 
                        action='store_true',
                        help='show all candidates')
        group.add_argument('-b', dest='best', 
                        action='store_true',
                        help='show only the best candidate (default)')
        parser.add_argument('-v', dest='verbose', 
                        action='store_true',
                        help='show points of each candidate')
        args = parser.parse_args()
        i = Ninja(args.compound)
        result = i.execute(args.candidates, args.verbose)
        print(result)

if __name__ == "__main__":
    app = Application(sys.argv)
    app.run()
