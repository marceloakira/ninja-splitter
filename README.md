# ninja-splitter

Tool to split compound words

## Documentation

* [Jupyter Notebook (portuguese)](http://nbviewer.jupyter.org/urls/gitlab.com/marceloakira/ninja-splitter/raw/master/NinjaSplitter.ipynb)

## Requirements

* Python >3.6
* Libraries listed in requirements.txt
* Hunspell

## Instalation

### Linux

* apt install hunspell hunspell-pt-br
* git clone https://gitlab.com/marceloakira/ninja-splitter/
* cd ninja-splitter
* pip install -r requirements.txt

## Examples

`./ninja.py -h ` : show help

`./ninja.py 'avidaébela'` : split 'avidaébela' to 'a vida é bela'

`./ninja.py -c 'avidaébela'` : show solution candidates to compound 'avidaébela'

`./ninja.py -c -v 'avidaébela'` : show solution candidates values to compound 'avidaébela'

Observation: the first execution will be slow, because ninja will generate models for stopwords, frequency and language model.
